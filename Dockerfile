FROM node:latest
WORKDIR /opt/oracle
COPY ./instantclient_19_3 ./client
RUN apt update 
RUN python3 --version
RUN apt install -y libaio1
RUN sh -c "echo /opt/oracle/client > /etc/ld.so.conf.d/oracle-instantclient.conf"
RUN ldconfig
RUN export LD_LIBRARY_PATH=/opt/oracle/client:$LD_LIBRARY_PATH
WORKDIR /usr/src/app
COPY ./package.json ./
COPY ./src ./
RUN npm i
CMD ["npm", "run", "start"]