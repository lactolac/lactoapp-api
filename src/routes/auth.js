const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');

const nanoid = require('nanoid').nanoid
const addHours = require('date-fns').addHours
const utcToZonedTime = require('date-fns-tz').utcToZonedTime
const SECRET = require('../utils').SECRET
const connection = require('../utils').CONNECTION
const checkRequired = require('../utils').checkRequired
const checkAuth = require('../utils').checkAuth
const INDEX = require('../utils').createIndex
const sendEmail = require('../tools/emailSender')

const knex = require('knex')({
  client: 'oracledb', connection
});

// LOGIN ENDPOINT
router.post('/login', async (req, res) => {
  // check if required fields are included
  const check = checkRequired(req.body, [{ name: 'email', type: 'email' }, 'password'])
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message })
  }

  const { email, password } = req.body

  // search email and validate its password
  const foundUser = await knex.select().table('USERS').where('EMAIL', email).first()
  if (foundUser == null || !bcrypt.compareSync(password, foundUser.PASSWORD)) {
    return res.status(403).json({ success: false, message: 'Usuario y/o contraseña incorrectos.' })
  }

  // create token
  const token = jwt.sign({
    uid: foundUser.ID
  }, SECRET, { expiresIn: '2h' })

  delete foundUser.PASSWORD // elimina el campo de la contraseña
  res.json({ success: true, token, changePasswordRequired: foundUser.CHANGE_PASSWORD == 1, data: foundUser })
})

// REGISTER ENDPOINT
router.post('/register', async (req, res) => {
  // check if required fields are included
  const check = checkRequired(req.body, [
    'names',
    'lastnames',
    { name: 'dob', type: 'date' },
    { name: 'email', type: 'email' },
    'cellphone',
    'password'
  ])
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message })
  }

  // get field values
  const { names, lastnames, dob, cellphone, email, password } = req.body

  // check if user already exist
  const foundUser = await knex.select().from('USERS').where('EMAIL', email).first()
  if (foundUser) {
    return res.status(400).json({ success: false, message: 'El usuario ya existe en el sistema.' })
  }

  try {
    // creates user
    await knex('USERS').insert({
      ID: await INDEX('USERS'),
      NAMES: names,
      LASTNAMES: lastnames,
      DOB: addHours(new Date(dob), 6),
      CELLPHONE: cellphone,
      EMAIL: email,
      PASSWORD: bcrypt.hashSync(password, 10),
      CREATED_AT: utcToZonedTime(new Date(), 'America/El_Salvador')
    })

    // Envia correo de vienvenido
    try {
      const emailContent = require('../emails/welcome.js')
      const sent = await sendEmail(email, 'LACTOLAC | Bienvenido.', emailContent(`${names} ${lastnames}`))
    } catch (error) {
      console.log(error)
    }

    res.json({
      success: true,
      message: 'El usuario ha sido creado exitosamente.'
    })
  } catch (error) {
    // when error happens when creating user
    console.error(error)
    return res.status(500).json({ success: false, message: 'Error al crear el usuario.' })
  }
})

// RESET PASSWORD
router.post('/reset', async (req, res) => {
  // check if required fields are included
  const check = checkRequired(req.body, ['email'])
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message })
  }

  // validate id user exist
  const { email } = req.body
  const foundUser = await knex.select().table('USERS').where('EMAIL', email).first()
  if (foundUser == null) {
    return res.status(400).json({ success: false, message: 'El usuario no existe en el sistema.' })
  }

  // generates a new password
  const newPassword = nanoid(10)

  // sends new password via email
  try {
    const user = await knex
      .select()
      .from('USERS')
      .where({ EMAIL: email })
      .first()

    const content = require('../emails/resetPassword.js')(newPassword, `${user.NAMES} ${user.LASTNAMES}`)
    await sendEmail(email, 'LACTOLAC | Reinicio de contraseña.', content)
  } catch (error) {
    console.log(error)
    return res.status(500).json({ success: false, message: `No se ha podido enviar la nueva contraseña por correo. ERROR: ${sent.message}` })
  }

  try {
    // updates user with new password
    await knex('USERS')
      .where('ID', foundUser.ID)
      .update({
        CHANGE_PASSWORD: 1,
        PASSWORD: bcrypt.hashSync(newPassword, 10)
      })

    res.json({
      success: true,
      message: 'La contraseña ha sido reiniciada correctamente.'
    })
  } catch (error) {
    console.error(error)
    return res.status(500).json({ success: false, message: 'Error al actualizar la contraseña.' })
  }
})

router.put('/reset/:id', checkAuth, async (req, res) => {
  // check if required fields are included
  const check = checkRequired(req.body, ['password'])
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message })
  }

  // validate id user exist
  const foundUser = await knex.select().table('USERS').where('ID', req.params.id).first()
  if (foundUser == null) {
    return res.status(400).json({ success: false, message: 'El usuario no existe en el sistema.' })
  }

  // sends new password via email
  try {
    const user = await knex
      .select()
      .from('USERS')
      .where({ ID: req.uid })
      .first()

    const content = require('../emails/passwordChanged.js')(`${user.NAMES} ${user.LASTNAMES}`)
    await sendEmail(foundUser.EMAIL, 'LACTOLAC | Contraseña actualizada.', content)
  } catch (error) {
    console.log(error)
    return res.status(500).json({ success: false, message: `No se ha podido enviar la nueva contraseña por correo. ERROR: ${sent.message}` })
  }

  try {
    // updates user with new password
    await knex('USERS')
      .where('ID', foundUser.ID)
      .update({
        CHANGE_PASSWORD: 0,
        PASSWORD: bcrypt.hashSync(req.body.password, 10)
      })

    res.json({
      success: true,
      message: 'La contraseña ha sido actualizada correctamente.'
    })
  } catch (error) {
    console.error(error)
    return res.status(500).json({ success: false, message: 'Error al actualizar la contraseña.' })
  }
})

module.exports = router