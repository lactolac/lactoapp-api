const express = require('express')
const router = express.Router()

const connection = require('../utils').CONNECTION

const knex = require('knex')({
  client: 'oracledb', connection
});

router.get('/', async (req, res) => {
  const settings = await knex.select().from('SETTINGS')
  const data = {}
  for (const item of settings) {
    data[item.META_KEY] = item.META_VALUE
  }
  res.json({
    success: true,
    message: 'Configuraciones de la APP',
    data
  })
})

router.get('/addresses', async (req, res) => {
  try {
    let countries = await knex
      .select()
      .from('COUNTRY')

    for (const country of countries) {
      const states = await knex.select().from('STATE').where('COUNTRY_ID', country.ID)
      country.states = states
      for (const state of states) {
        state.cities = await knex.select().from('CITY').where('STATE_ID', state.ID)
      }
    }

    res.json({
      success: true,
      message: 'Listado de paises, departamentos y municipios.',
      countries
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({
      success: false,
      message: 'Error al obtener departamentos y municipios.',
    })
  }
})

module.exports = router