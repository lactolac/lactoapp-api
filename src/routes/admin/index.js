const express = require("express");
const router = express.Router();
const checkAdminAuth = require("../../utils").checkAdminAuth;

// ROUTES
const auth = require("./routes/auth");
const orders = require("./routes/order");
const drivers = require("./routes/drivers");
const statistics = require("./routes/statistics");

// ROUTES FOR ADMIN
router.use("/auth", auth);
router.use("/orders", checkAdminAuth, orders);
router.use("/drivers", checkAdminAuth, drivers);
router.use("/statistics", checkAdminAuth, statistics);

module.exports = router;
