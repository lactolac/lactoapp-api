const express = require("express");
const router = express.Router({ mergeParams: true });
const connection = require("../../../../utils").CONNECTION;
const { addHours, endOfDay } = require("date-fns");
const { checkRequired } = require("../../../../utils");

const knex = require("knex")({
  client: "oracledb",
  connection,
});

router.get("/payments", async (req, res) => {
  const check = checkRequired(
    req.query,
    [
      { name: "from", type: "date", optional: true },
      { name: "to", type: "date", optional: true },
    ],
    true
  );
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message });
  }

  const { from, to } = req.query;

  // obtiene todas las ordenes pagadas
  const orders = await knex("ORDERS")
    .select()
    .where("CREATED_AT", ">=", new Date(from))
    .where("CREATED_AT", "<=", addHours(endOfDay(new Date(to)), 6));

  for (const order of orders) {
    order.STATUSES = await knex("ORDER_STATUSES")
      .select()
      .where({ ORDER_ID: order.ID })
      .orderBy("CREATED_AT", "DESC");
  }

  const paidCardOrders = orders.filter(
    (o) => o.STATUSES.find((s) => s.STATUS_ID == 2) && o.PAYMENT_TYPE == 1
  );

  const paidCashOrders = orders.filter(
    (o) => o.STATUSES[0].STATUS_ID == 4 && o.PAYMENT_TYPE == 0
  );

  const rejectedOrders = orders.filter(
    (o) => o.STATUSES[0].STATUS_ID == 7 && o.PAYMENT_TYPE == 1
  );

  const cancelledOrders = orders.filter((o) => o.STATUSES[0].STATUS_ID == 5);

  return res.json({
    cardPayments: {
      count: paidCardOrders.length,
      total: paidCardOrders.reduce(
        (a, b) => {
          return {
            SUBTOTAL: a.SUBTOTAL + b.SUBTOTAL,
          };
        },
        { SUBTOTAL: 0 }
      ).SUBTOTAL,
      orders: paidCardOrders,
    },
    cashPayments: {
      count: paidCashOrders.length,
      total: paidCashOrders.reduce(
        (a, b) => {
          return {
            SUBTOTAL: a.SUBTOTAL + b.SUBTOTAL,
          };
        },
        { SUBTOTAL: 0 }
      ).SUBTOTAL,
      orders: paidCashOrders,
    },
    rejectedPayments: {
      count: rejectedOrders.length,
      total: rejectedOrders.reduce(
        (a, b) => {
          return {
            SUBTOTAL: a.SUBTOTAL + b.SUBTOTAL,
          };
        },
        { SUBTOTAL: 0 }
      ).SUBTOTAL,
      orders: rejectedOrders,
    },
    cancelledOrders: {
      count: cancelledOrders.length,
      total: cancelledOrders.reduce(
        (a, b) => {
          return {
            SUBTOTAL: a.SUBTOTAL + b.SUBTOTAL,
          };
        },
        { SUBTOTAL: 0 }
      ).SUBTOTAL,
      orders: cancelledOrders,
    },
  });
});

module.exports = router;
