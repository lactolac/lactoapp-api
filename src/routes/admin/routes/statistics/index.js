const express = require("express");
const router = express.Router();

// ROUTES
const statistics = require("./statistics");

router.use("/", statistics);

module.exports = router;
