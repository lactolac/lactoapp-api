const express = require("express");
const router = express.Router({ mergeParams: true });
const utcToZonedTime = require("date-fns-tz").utcToZonedTime;
const {
  format,
  differenceInMinutes,
  endOfDay,
  addHours,
  addDays,
  isWithinInterval,
} = require("date-fns");
const axios = require("axios");

const connection = require("../../../../utils").CONNECTION;
const INDEX = require("../../../../utils").createIndex;
const checkRequired = require("../../../../utils").checkRequired;

const knex = require("knex")({
  client: "oracledb",
  connection,
});

router.get("/", async (req, res) => {
  const check = checkRequired(
    req.query,
    [
      { name: "status", optional: true },
      { name: "from", type: "date", optional: true },
      { name: "to", type: "date", optional: true },
    ],
    true
  );
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message });
  }

  const { status, from, to } = req.query;

  // Obtiene las ordenes
  let orders = null;
  if (from != null && to != null) {
    orders = await knex
      .select()
      .from("ORDERS")
      .where("CREATED_AT", ">=", new Date(from))
      .where("CREATED_AT", "<=", addHours(endOfDay(new Date(to)), 6))
      .orderBy("CREATED_AT", "DESC");
  } else {
    orders = await knex.select().from("ORDERS").orderBy("CREATED_AT", "DESC");
  }

  // para cada orden
  for (const order of orders) {
    // obtiene los detalles
    order.DETAILS = await knex
      .select(
        "ORDERDETAILS.ID",
        "ORDERDETAILS.ORDER_ID",
        "ORDERDETAILS.PRODUCT_ID",
        "ORDERDETAILS.QUANTITY",
        "ORDERDETAILS.CREATED_AT",
        "ORDERDETAILS.UPDATED_AT",
        "ORDERDETAILS.AMOUNT",
        "ORDERDETAILS.EDIT",
        "PRODUCTOS.DETALLE AS PRODUCT_NAME"
      )
      .from("ORDERDETAILS")
      .where({ ORDER_ID: order.ID })
      .leftJoin("PRODUCTOS", "ORDERDETAILS.PRODUCT_ID", "PRODUCTOS.PRODUCTO");

    order.CREATED_AT = format(new Date(order.CREATED_AT), "dd/MM/yyyy hh:mm a");
    order.EDIT = order.EDIT == 1;

    order.DETAILS = order.DETAILS.map((o) => {
      return {
        ...o,
        EDIT: o.EDIT == 1,
      };
    });

    // obtiene los estados
    const STATUSES = await knex
      .select("STATUS_ID", "CREATED_AT", "NAME", "COMMENTS", "REASON")
      .from("ORDER_STATUSES")
      .where({ ORDER_ID: order.ID })
      .leftJoin("STATUSES", "ORDER_STATUSES.STATUS_ID", "STATUSES.ID")
      .orderBy("CREATED_AT", "ASC");
    order.STATUSES = STATUSES;
    order.STATUS = STATUSES[STATUSES.length - 1].STATUS_ID;
    order.STATUS_NAME = STATUSES[STATUSES.length - 1].NAME;
    order.STATUS_DATETIME = format(
      new Date(STATUSES[STATUSES.length - 1].CREATED_AT),
      "dd/MM/yyyy hh:mm a"
    );

    // obtiene la direccion
    const address_details = await knex
      .select()
      .from("ADDRESSES")
      .where({ ID: order.ADDRESS_ID })
      .first();

    address_details.COUNTRY_NAME = (
      await knex
        .select()
        .from("COUNTRY")
        .where({ ID: address_details.COUNTRY_ID })
        .first()
    ).NAME;
    address_details.STATE_NAME = (
      await knex
        .select()
        .from("STATE")
        .where({ ID: address_details.STATE_ID })
        .first()
    ).NAME;
    address_details.CITY_NAME = (
      await knex
        .select()
        .from("CITY")
        .where({ ID: address_details.CITY_ID })
        .first()
    ).NAME;
    order.ADDRESS_DETAILS = address_details;
  }

  if (status) {
    switch (status) {
      case "active":
        orders = orders.filter((o) => [1, 2, 3].includes(o.STATUS));
        break;
      case "others":
        orders = orders.filter((o) => [4, 5, 6, 7].includes(o.STATUS));
        break;
    }
  }

  res.json({
    success: true,
    message: `List of orders`,
    count: orders.length,
    history: orders,
  });
});

router.get("/:id", async (req, res) => {
  const { id } = req.params;
  const order = await knex.select().from("ORDERS").where({ ID: id }).first();

  order.CREATED_AT = format(new Date(order.CREATED_AT), "dd/MM/yyyy hh:mm a");
  const details = await knex
    .select()
    .from("ORDERDETAILS")
    .where({ ORDER_ID: order.ID });

  for (const detail of details) {
    detail.PRODUCT_NAME = (
      await knex
        .select()
        .from("PRODUCTOS")
        .where({ PRODUCTO: detail.PRODUCT_ID })
        .first()
    ).DETALLE;
  }
  order.DETAILS = details;

  // obtiene los estados
  const STATUSES = await knex
    .select("STATUS_ID", "CREATED_AT", "NAME", "COMMENTS", "REASON")
    .from("ORDER_STATUSES")
    .where({ ORDER_ID: order.ID })
    .leftJoin("STATUSES", "ORDER_STATUSES.STATUS_ID", "STATUSES.ID")
    .orderBy("CREATED_AT", "ASC");
  order.STATUSES = STATUSES;
  order.STATUS = STATUSES[STATUSES.length - 1].STATUS_ID;
  order.STATUS_NAME = STATUSES[STATUSES.length - 1].NAME;
  order.STATUS_DATETIME = STATUSES[STATUSES.length - 1].CREATED_AT;

  const address_details = await knex
    .select()
    .from("ADDRESSES")
    .where({ ID: order.ADDRESS_ID })
    .first();

  address_details.COUNTRY_NAME = (
    await knex
      .select()
      .from("COUNTRY")
      .where({ ID: address_details.COUNTRY_ID })
      .first()
  ).NAME;
  address_details.STATE_NAME = (
    await knex
      .select()
      .from("STATE")
      .where({ ID: address_details.STATE_ID })
      .first()
  ).NAME;
  address_details.CITY_NAME = (
    await knex
      .select()
      .from("CITY")
      .where({ ID: address_details.CITY_ID })
      .first()
  ).NAME;
  order.ADDRESS_DETAILS = address_details;

  res.json({
    success: true,
    order: order,
  });
});

router.delete("/:id", async (req, res) => {
  // verifica los campos requeridos
  const check = checkRequired(req.body, ["reason"]);
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message });
  }

  // obtiene la data requerida
  const { id } = req.params;
  const { reason } = req.body;

  // Obtiene la orden
  const order = await knex().select().from("ORDERS").where({ ID: id }).first();

  // Verifica si existe
  if (!order) {
    return res
      .status(400)
      .json({ success: false, message: "La orden solicitada no existe." });
  }

  // Se permite si la orden esta en estado 1
  const statuses = await knex
    .select("STATUS_ID")
    .from("ORDER_STATUSES")
    .where({ ORDER_ID: order.ID })
    .orderBy("CREATED_AT", "ASC");

  if (statuses[statuses.length - 1].STATUS_ID != 1) {
    if(order.PAYMENT_TYPE==1){
    return res
      .status(400)
      .json({
        success: false,
        message: "La orden solicitada ya esta procesada.",
      });}
  }

  // Cancela la orden
  try {
    await knex("ORDERS")
      .where("ID", id)
      .update({
        UPDATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
      });

    await knex("ORDER_STATUSES").insert({
      ID: await INDEX("ORDER_STATUSES"),
      ORDER_ID: id,
      STATUS_ID: 5,
      REASON: reason,
      CREATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
    });
  } catch (error) {
    console.error(error);
    return res.status(401).json({
      success: false,
      message: "Error al actualizar los detalles de la orden.",
    });
  }

  // Envia correo de confirmacion
  const user = await knex
    .select()
    .from("USERS")
    .where({ ID: order.USER_ID })
    .first();
  const sendEmail = require("../../../../tools/emailSender");
  const emailContent = require("../../../../emails/orderCancelledAdmin");
  await sendEmail(
    user.EMAIL,
    "LACTOLAC | Orden cancelada.",
    emailContent(`${user.NAMES} ${user.LASTNAMES}`, order.CODE, reason)
  );

  return res.json({
    success: true,
    message: "Se ha actualizado la orden con éxito.",
  });
});

router.post("/reserve/:id", async (req, res) => {
  const { id } = req.params;

  // Obtiene la orden
  const order = await knex().select().from("ORDERS").where({ ID: id }).first();

  // Verifica si existe
  if (!order) {
    return res
      .status(400)
      .json({ success: false, message: "La orden solicitada no existe." });
  }

  // Verifica si esta en abierta y la ultima actualizcion es menor a 15 minutos
  const lastupdated = differenceInMinutes(
    new Date(),
    new Date(order.UPDATED_AT)
  );
  if (order.OPENED != null) {
    if (order.OPENED != req.uid && lastupdated < 15)
      return res
        .status(200)
        .json({
          success: false,
          message: "La orden esta siendo trabajada por otro usuario.",
        });
  }

  try {
    // Actualiza el opened
    await knex("ORDERS")
      .where({ ID: id })
      .update({
        OPENED: req.uid,
        UPDATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
      });

    return res
      .status(200)
      .json({ success: true, message: "La orden puede ser procesada." });
  } catch (error) {
    return res
      .status(401)
      .json({ success: false, message: "Error al actualizar la orden." });
  }
});



router.post("/cash/:id", async (req, res) => {
  const { id } = req.params;

  // Obtiene la orden
  const order = await knex().select().from("ORDERS").where({ ID: id }).first();

  // Verifica si existe
  if (!order) {
    return res
      .status(400)
      .json({ success: false, message: "La orden solicitada no existe." });
  }

  try {
    await knex("ORDERS")
      .where({ ID: id })
      .update({
        PAYMENT_TYPE: 0,
        CARD_ID: null,
      });

    return res
      .status(200)
      .json({ success: true, message: "Cambiado a efectivo con exito." });
  } catch (error) {
    return res
      .status(401)
      .json({ success: false, message: "Error al cambiar a efectivo la orden." });
  }
});


router.put("/process/:id", async (req, res) => {
  const { id } = req.params;

  // verifica los campos requeridos
  const checkHeader = checkRequired(req.body, [
    "changed",
    "admin",
    "driver",
    "subtotal",
    { name: "details", type: "array" },
  ]);
  if (!checkHeader.success) {
    return res
      .status(400)
      .json({ success: false, message: checkHeader.message });
  }

  // obtiene la data requerida
  const { changed, driver, admin, subtotal, details } = req.body;

  // verifica que la orden exista
  const order = await knex.select().from("ORDERS").where({ ID: id }).first();

  // Verifica que la orden exista
  if (!order) {
    return res
      .status(400)
      .json({ success: false, message: "La orden solicitada no existe." });
  }

  // Se permite si la orden esta en estado 1
  const statuses = await knex
    .select("STATUS_ID")
    .from("ORDER_STATUSES")
    .where({ ORDER_ID: order.ID })
    .orderBy("CREATED_AT", "ASC");

  if (statuses[statuses.length - 1].STATUS_ID != 1) {
    return res
      .status(400)
      .json({
        success: false,
        message: "La orden solicitada ya esta procesada.",
      });
  }

  // Si la orden cambio, actualiza los detalles.
  if (changed) {
    // Elimina todos los detalles
    try {
      await knex("ORDERDETAILS").where({ ORDER_ID: id }).del();
    } catch (error) {
      console.error(error);
      return res.status(401).json({
        success: false,
        message: "Error al actualizar los detalles de la orden.",
      });
    }
    // Inserta los detalles actualizados
    try {
      for (const detail of JSON.parse(details)) {
        await knex("ORDERDETAILS").insert({
          ID: await INDEX("ORDERDETAILS"),
          ORDER_ID: id,
          PRODUCT_ID: detail.product,
          QUANTITY: detail.quantity,
          AMOUNT: detail.amount,
          CREATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
          UPDATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
        });
      }
    } catch (error) {
      console.error(error);
      return res.status(401).json({
        success: false,
        message: "Error al actualizar los detalles de la orden.",
      });
    }

    // Actualizar el total y updated
    try {
      await knex("ORDERS")
        .update({
          SUBTOTAL: subtotal,
          UPDATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
        })
        .where({ ID: id });
    } catch (error) {
      console.error(error);
      return res.status(401).json({
        success: false,
        message: "Error al actualizar el encabezado de la orden.",
      });
    }
  }

  // Procesa cobro electronico en caso que sea cobro electronico
  if (order.PAYMENT_TYPE == 1) {
    if (order.CARD_ID == null) {
      return res
        .status(400)
        .json({
          success: false,
          message: "La orden solicitada ya esta procesada.",
        });
    }
    try {
      await axios.post("http://192.168.101.100:4004/requestPayment", {
        // cardId: order.CARD_ID,
        cardId: "09ffb098-ba16-11ea-afed-02420aff802e",
        amount: subtotal,
      });
    } catch (error) {
      return res
        .status(500)
        .json({ success: false, message: error.response.data });
    }
  }

  // Agrega driver y relaciona tienda
  try {
    await knex("ORDERS")
      .where("ID", id)
      .update({
        DRIVER_ID: driver,
        ADMIN_ID: admin,
        UPDATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
      });

    await knex("ORDER_STATUSES").insert({
      ID: await INDEX("ORDER_STATUSES"),
      ORDER_ID: id,
      STATUS_ID: 2,
      CREATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
    });
  } catch (error) {
    console.error(error);
    return res.status(401).json({
      success: false,
      message: "Error al actualizar los detalles de la orden.",
    });
  }

  // Envia correo de confirmacion
  const user = await knex
    .select()
    .from("USERS")
    .where({ ID: order.USER_ID })
    .first();
  const sendEmail = require("../../../../tools/emailSender");
  const emailContent = require("../../../../emails/orderProcessed");
  await sendEmail(
    user.EMAIL,
    "LACTOLAC | Orden procesada.",
    emailContent(`${user.NAMES} ${user.LASTNAMES}`, details, subtotal)
  );

  return res.json({
    success: true,
    message: "Se ha actualizado la orden con éxito.",
  });
});

router.post("/dispatch/:id", async (req, res) => {
  // obtiene el id
  const { id } = req.params;

  // Verifica los campos requeridos
  const check = checkRequired(req.body, ["eta", "driver"]);
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message });
  }

  // obtiene la data requerida
  const { eta, driver } = req.body;

  // obtiene la orden
  const order = await knex("ORDERS").select().where({ ID: id }).first();

  // verifica que la orden exista
  if (!order) {
    return res
      .status(400)
      .json({ success: false, message: "La orden solicitada no existe." });
  }

  // verifica que la orden se pueda enviar en ruta
  const statuses = await knex
    .select("STATUS_ID")
    .from("ORDER_STATUSES")
    .where({ ORDER_ID: order.ID })
    .orderBy("CREATED_AT", "ASC");

  if (statuses[statuses.length - 1].STATUS_ID != 2) {
    return res
      .status(400)
      .json({
        success: false,
        message: "La orden solicitada no se puede enviar en ruta.",
      });
  }

  // actualiza de en ruta
  try {
    await knex("ORDERS")
      .where("ID", id)
      .update({
        ETA: eta,
        DRIVER_ID: driver,
        UPDATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
      });

    await knex("ORDER_STATUSES").insert({
      ID: await INDEX("ORDER_STATUSES"),
      ORDER_ID: id,
      STATUS_ID: 3,
      CREATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
    });

    return res.json({
      success: true,
      message: "La orden ha sido marcada en ruta.",
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Error al poner en ruta la orden.",
    });
  }
});

router.get("/statuses", async (req, res) => {
  const statuses = await knex("STATUSES").select();
  res.json({
    success: true,
    message: `List of statuses`,
    statuses,
  });
});

router.post("/revert/:id", async (req, res) => {
  const { id } = req.params;

  // Obtiene la orden
  const order = await knex().select().from("ORDERS").where({ ID: id }).first();

  // Verifica si existe
  if (!order) {
    return res
      .status(400)
      .json({ success: false, message: "La orden solicitada no existe." });
  }

  const STATUSES = await knex
    .select("STATUS_ID", "CREATED_AT", "NAME", "COMMENTS", "REASON")
    .from("ORDER_STATUSES")
    .where({ ORDER_ID: order.ID })
    .leftJoin("STATUSES", "ORDER_STATUSES.STATUS_ID", "STATUSES.ID")
    .orderBy("CREATED_AT", "ASC");
  order.STATUSES = STATUSES;
  const currentStatus = STATUSES[STATUSES.length - 1];
  const priorStatus =
    STATUSES.length > 1 ? STATUSES[STATUSES.length - 2] : null;

  if (priorStatus == null) {
    return res
      .status(400)
      .json({
        success: false,
        message: "No se puede revertir el estado actual de la orden.",
      });
  }

  if ([3, 4, 5].includes(currentStatus.STATUS_ID)) {
    await knex("ORDER_STATUSES").insert({
      ID: await INDEX("ORDER_STATUSES"),
      ORDER_ID: id,
      STATUS_ID: priorStatus.STATUS_ID,
      REASON: "Estado asignado por una reversion en el sistema.",
      CREATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
    });

    res.json({
      success: true,
      message: "Se ha realizado la reversion exitosamente.",
    });
  } else {
    return res
      .status(400)
      .json({
        success: false,
        message: "No se puede revertir el estado actual de la orden.",
      });
  }
});

module.exports = router;
