const express = require('express')
const router = express.Router()

// ROUTES
const drivers = require('./drivers')

router.use('/', drivers)

module.exports = router