const express = require('express')
const router = express.Router()

const connection = require('../../../../utils').CONNECTION

const knex = require('knex')({
  client: 'oracledb', connection
});

router.get('/', async (req, res) => {
  let drivers = await knex.select().from('DRIVER_USERS')

  res.json({
    success: true,
    message: 'List of drivers',
    count: drivers.length,
    data: drivers
  })
})

module.exports = router