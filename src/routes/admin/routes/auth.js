const express = require('express')
const router = express.Router()
const checkRequired = require('../../../utils').checkRequired
const bcrypt = require('bcrypt');
const connection = require('../../../utils').CONNECTION
const jwt = require('jsonwebtoken')
const SECRET = require('../../../utils').SECRET

const knex = require('knex')({
  client: 'oracledb', connection
});

router.post('/login', async (req, res) => {
  const check = checkRequired(req.body, ['username', 'password'])
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message })
  }

  const { username, password } = req.body

  const foundUser = await knex.select().table('ADMIN_USERS').where('USERNAME', username).first()
  if (foundUser == null || !bcrypt.compareSync(password, foundUser.PASSWORD)) {
    return res.status(403).json({ success: false, message: 'Usuario y/o contraseña incorrectos.' })
  }

  const token = jwt.sign({
    uid: foundUser.ID,
    sid: foundUser.STORE_ID
  }, SECRET, { expiresIn: '2h' })

  delete foundUser.PASSWORD // elimina el campo de la contraseña
  res.json({ success: true, token, changePasswordRequired: foundUser.CHANGE_PASSWORD == 1, data: foundUser })
})

module.exports = router