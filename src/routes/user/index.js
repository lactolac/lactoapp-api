const express = require('express')
const router = express.Router()

// ROUTES
const users = require('./users')
const addresses = require('./addresses')
const cards = require('./cards')

router.use('/', users)
router.use('/addresses', addresses)
router.use('/cards', cards)

module.exports = router