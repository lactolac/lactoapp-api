const express = require('express')
const router = express.Router({ mergeParams: true })

const connection = require('../../utils').CONNECTION
const INDEX = require('../../utils').createIndex
const checkRequired = require('../../utils').checkRequired

const knex = require('knex')({
  client: 'oracledb', connection
});

router.get('/', async (req, res) => {
  const user_id = req.uid
  const addresses = await knex
    .select()
    .from('ADDRESSES')
    .where({ USER_ID: user_id })
    .whereNot({ DELETED: 1 })

  for (const address of addresses) {
    address.COUNTRY_NAME = (await knex.select().from('COUNTRY').where({ ID: address.COUNTRY_ID }).first()).NAME
    address.STATE_NAME = (await knex.select().from('STATE').where({ ID: address.STATE_ID }).first()).NAME
    address.CITY_NAME = (await knex.select().from('CITY').where({ ID: address.CITY_ID }).first()).NAME
  }

  res.json({
    success: true,
    message: `List of addresses for user id: ${user_id}`,
    count: addresses.length,
    data: addresses
  })
})

router.get('/:id', async (req, res) => {
  const { id } = req.params
  const user_id = req.uid
  try {
    const address = await knex
      .select()
      .from('ADDRESSES')
      .where({ USER_ID: user_id, ID: id })
      .whereNot({ DELETED: 1 })
      .first()

    if (address) {
      address.COUNTRY_NAME = (await knex.select().from('COUNTRY').where({ ID: address.COUNTRY_ID }).first()).NAME
      address.STATE_NAME = (await knex.select().from('STATE').where({ ID: address.STATE_ID }).first()).NAME
      address.CITY_NAME = (await knex.select().from('CITY').where({ ID: address.CITY_ID }).first()).NAME
    }

    res.json({
      success: true,
      message: address ? `Address id: ${id} for user id: ${user_id}` : `No address with id: ${id} found`,
      data: address
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({
      success: false,
      message: 'Error getting the address',
    })
  }
})

router.post('/', async (req, res) => {
  // check required fields
  const check = checkRequired(req.body, [
    'longitude',
    'latitude',
    'address1',
    { name: 'address2', optional: true },
    'country',
    'state',
    'city'
  ])
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message })
  }

  // get fields to update
  const { longitude, latitude, address1, address2, country, state, city } = req.body
  console.log(longitude, latitude, address1, address2, country, state, city)


  // crea
  try {
    const ID = await INDEX('ADDRESSES')
    await knex('ADDRESSES')
      .insert({
        ID,
        USER_ID: req.uid,
        LONGITUDE: longitude,
        LATITUDE: latitude,
        ADDRESS1: address1,
        ADDRESS2: address2,
        COUNTRY_ID: country,
        STATE_ID: state,
        CITY_ID: city
      })

    res.json({
      success: true,
      message: 'la dirección del usuario ha sido creada con éxito.',
      id: ID
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({
      success: false,
      message: 'Error al crear la dirección del usuario.',
    })
  }
})

router.put('/:id', async (req, res) => {
  const { id } = req.params
  // check required fields
  const check = checkRequired(req.body, [
    { name: 'longitude', optional: true },
    { name: 'latitude', optional: true },
    { name: 'address1', optional: true },
    { name: 'address2', optional: true },
    { name: 'country', optional: true },
    { name: 'state', optional: true },
    { name: 'city', optional: true }
  ])
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message })
  }

  // get fields to update
  const { longitude, latitude, address1, address2, country, state, city } = req.body

  // actualiza
  try {
    const values = {}
    if (longitude) { values.LONGITUDE = longitude }
    if (latitude) { values.LATITUDE = latitude }
    if (address1) { values.ADDRESS1 = address1 }
    if (address2) { values.ADDRESS2 = address2 }
    if (country) { values.COUNTRY_ID = country }
    if (state) { values.STATE_ID = state }
    if (city) { values.CITY_ID = city }

    await knex('ADDRESSES')
      .where('ID', id)
      .update(values)

    res.json({
      success: true,
      message: 'la dirección del usuario ha sido actualizada exitosamente.'
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({
      success: false,
      message: 'Error al actualizar la dirección del usuario.',
    })
  }
})

router.delete('/:id', async (req, res) => {
  const { id } = req.params
  try {
    await knex('ADDRESSES')
      .where('ID', id)
      .update({ 'DELETED': 1 })

    res.json({
      success: true,
      message: 'la dirección del usuario ha sido eliminada exitosamente.'
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({
      success: false,
      message: 'Error al eliminar la dirección del usuario.',
    })
  }
})

module.exports = router