const express = require('express')
const router = express.Router()

const connection = require('../../utils').CONNECTION
const checkRequired = require('../../utils').checkRequired
const addHours = require('date-fns').addHours

const knex = require('knex')({
  client: 'oracledb', connection
});

// OBTIENE USUARIO
router.get('/', async (req, res) => {
  // obtiene al ususario
  const user = await knex
    .select()
    .from('USERS')
    .where({ ID: req.uid })
    .first()

  if (user) {
    // si encuenta al usuario
    // elimina el campo de la contraseña
    delete user.PASSWORD
    res.json({
      success: true,
      message: 'User found',
      data: user
    })
  } else {
    res.status(400).json({
      success: false,
      message: 'User not found',
    })
  }

})

router.put('/', async (req, res) => {
  // checks if user exist
  const user = await knex
    .select()
    .from('USERS')
    .where({ ID: req.uid })
    .first()
  if (!user) {
    res.status(400).json({
      success: false,
      message: 'User not found',
    })
  }

  // check required fields
  const check = checkRequired(req.body, [
    { name: 'names', optional: true },
    { name: 'lastnames', optional: true },
    { name: 'dob', type: 'date', optional: true },
    { name: 'cellphone', optional: true },
    { name: 'email', type: 'email', optional: true }
  ])
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message })
  }

  // get fields to update
  const { names, lastnames, dob, cellphone, email } = req.body

  // Actualiza
  try {
    const values = {}
    if (names) { values.NAMES = names }
    if (lastnames) { values.LASTNAMES = lastnames }
    if (dob) { values.DOB = addHours(new Date(dob), 6) }
    if (cellphone) { values.CELLPHONE = cellphone }
    if (email) { values.EMAIL = email }
    values.UPDATED_AT = new Date()

    await knex('USERS')
      .where('ID', req.uid)
      .update(values)

    res.json({
      success: true,
      message: 'El usuario ha sido actualizado exitosamente.'
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({
      success: false,
      message: 'Error al actualizar el usuario.',
    })
  }
})

module.exports = router
