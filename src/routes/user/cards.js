const express = require('express')
const router = express.Router({ mergeParams: true })
const axios = require('axios')

const connection = require('../../utils').CONNECTION
const INDEX = require('../../utils').createIndex
const checkRequired = require('../../utils').checkRequired
const uuid = require('uuid').v1;
const utcToZonedTime = require('date-fns-tz').utcToZonedTime
const splitEvery = require('ramda').splitEvery

const knex = require('knex')({
  client: 'oracledb', connection
});

router.get('/', async (req, res) => {
  const user_id = req.uid
  const cards = await knex
    .select()
    .from('USER_CARDS')
    .where({ USER_ID: user_id })

  res.json({
    success: true,
    message: `List of cards for user id: ${user_id}`,
    count: cards.length,
    cards
  })
})

router.get('/:id', async (req, res) => {
  const { id } = req.params
  const user_id = req.uid
  try {
    const cards = await knex
      .select()
      .from('USER_CARDS')
      .where({ USER_ID: user_id, ID: id })
      .first()

    res.json({
      success: true,
      message: cards ? `Card id: ${id} for user id: ${user_id}` : `No cards with id: ${id} for user id: ${user_id} found`,
      cards
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({
      success: false,
      message: 'Error getting the cards',
    })
  }
})

router.post('/', async (req, res) => {
  // check required fields
  const check = checkRequired(req.body, ['number', 'name', 'exp', 'cvv'])
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message })
  }

  // get fields
  const { number, name, exp, cvv } = req.body

  // crea
  try {
    const { data } = await axios.post('http://192.168.101.100:4004/register', {
      cardName: name,
      cardNumber: number,
      expiration: exp,
      security: cvv
    })

    const ID = await INDEX('USER_CARDS')
    const CARD_ID = data
    const MASK = ['XXXX', 'XXXX', 'XXXX'].concat(splitEvery(4, number)[3]).join('-')
    await knex('USER_CARDS')
      .insert({
        ID,
        CARD_ID,
        NAME: name,
        MASK,
        EXP: exp,
        CREATED_AT: utcToZonedTime(new Date(), 'America/El_Salvador'),
        USER_ID: req.uid
      })

    res.json({
      success: true,
      message: 'La tarjeta del usuario ha sido registrada con éxito.',
      id: ID,
      card_id: CARD_ID,
      mask: MASK
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({
      success: false,
      message: 'Error al registrar la tarjeta del usuario.',
    })
  }
})

module.exports = router