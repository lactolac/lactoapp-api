const express = require('express')
const router = express.Router()

const connection = require('../utils').CONNECTION

const knex = require('knex')({
  client: 'oracledb', connection
});

router.get('/', async (req, res) => {
  let products = await knex.select().from('PRODUCTOS')

  res.json({
    success: true,
    message: 'List of products',
    count: products.length,
    data: products
  })
})

module.exports = router