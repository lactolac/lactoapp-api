const express = require("express");
const router = express.Router({ mergeParams: true });
const utcToZonedTime = require("date-fns-tz").utcToZonedTime;

const connection = require("../../utils").CONNECTION;
const INDEX = require("../../utils").createIndex;
const CODE = require("../../utils").createCode;
const checkRequired = require("../../utils").checkRequired;

const knex = require("knex")({
  client: "oracledb",
  connection,
});

router.get("/", async (req, res) => {
  const user_id = req.uid;
  const findStatus = req.query.status;

  // Obtiene las ordenes
  let orders = await knex.select().from("ORDERS").where({ USER_ID: user_id });

  // para cada orden
  for (const order of orders) {
    // obtiene los detalles
    order.DETAILS = await knex
      .select()
      .from("ORDERDETAILS")
      .where({ ORDER_ID: order.ID });

    // obtiene los estados
    const STATUSES = await knex
      .select("STATUS_ID", "CREATED_AT", "NAME", "COMMENTS", "REASON")
      .from("ORDER_STATUSES")
      .where({ ORDER_ID: order.ID })
      .leftJoin("STATUSES", "ORDER_STATUSES.STATUS_ID", "STATUSES.ID")
      .orderBy("CREATED_AT", "ASC");
    order.STATUSES = STATUSES;
    order.STATUS = STATUSES[STATUSES.length - 1].STATUS_ID;
    order.STATUS_NAME = STATUSES[STATUSES.length - 1].NAME;
    order.STATUS_DATETIME = STATUSES[STATUSES.length - 1].CREATED_AT;

    // obtiene la direccion
    order.ADDRESS_DETAILS = await knex
      .select()
      .from("ADDRESSES")
      .where({ ID: order.ADDRESS_ID });
  }

  if (findStatus) {
    switch (findStatus) {
      case "active":
        orders = orders.filter((o) => [1, 2, 3].includes(o.STATUS));
        break;
      case "others":
        orders = orders.filter((o) => [4, 5, 6, 7].includes(o.STATUS));
        break;
    }
  }

  res.json({
    success: true,
    message: `List of orders for user id: ${user_id}`,
    count: orders.length,
    history: orders,
  });
});

router.get("/:id", async (req, res) => {
  const { id } = req.params;
  const user_id = req.uid;
  const order = await knex
    .select()
    .from("ORDERS")
    .where({ USER_ID: user_id, ID: id })
    .first();

  const details = await knex
    .select()
    .from("ORDERDETAILS")
    .where({ ORDER_ID: order.ID });

  for (const detail of details) {
    detail.PRODUCT_NAME = (
      await knex
        .select()
        .from("PRODUCTOS")
        .where({ PRODUCTO: detail.PRODUCT_ID })
        .first()
    ).DETALLE;
  }
  order.DETAILS = details;

  // obtiene los estados
  const STATUSES = await knex
    .select("STATUS_ID", "CREATED_AT", "NAME", "COMMENTS", "REASON")
    .from("ORDER_STATUSES")
    .where({ ORDER_ID: order.ID })
    .leftJoin("STATUSES", "ORDER_STATUSES.STATUS_ID", "STATUSES.ID")
    .orderBy("CREATED_AT", "ASC");
  order.STATUSES = STATUSES;
  order.STATUS = STATUSES[STATUSES.length - 1].STATUS_ID;
  order.STATUS_NAME = STATUSES[STATUSES.length - 1].NAME;
  order.STATUS_DATETIME = STATUSES[STATUSES.length - 1].CREATED_AT;

  const address_details = await knex
    .select()
    .from("ADDRESSES")
    .where({ ID: order.ADDRESS_ID })
    .first();

  address_details.COUNTRY_NAME = (
    await knex
      .select()
      .from("COUNTRY")
      .where({ ID: address_details.COUNTRY_ID })
      .first()
  ).NAME;
  address_details.STATE_NAME = (
    await knex
      .select()
      .from("STATE")
      .where({ ID: address_details.STATE_ID })
      .first()
  ).NAME;
  address_details.CITY_NAME = (
    await knex
      .select()
      .from("CITY")
      .where({ ID: address_details.CITY_ID })
      .first()
  ).NAME;
  order.ADDRESS_DETAILS = address_details;

  res.json({
    success: true,
    message: `List of orders for user id: ${user_id}`,
    order: order,
  });
});

router.post("/", async (req, res) => {
  // Verifica parametros de encabezado
  const check = checkRequired(req.body, [
    "address",
    "recipient_name",
    "recipient_phone",
    "payment_type",
    "subtotal",
    "tax",
    "card_id",
    { name: "details", type: "array" },
  ]);
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message });
  }

  const {
    address,
    recipient_name,
    recipient_phone,
    payment_type,
    subtotal,
    tax,
    card_id,
    details,
  } = req.body;

  if (payment_type == 1 && (card_id == null || card_id == "")) {
    return res.status(400).json({
      success: false,
      message: "El metodo de pago requiere un ID de tarjeta.",
    });
  }

  let ORDER_ID = null;
  // Inserta encabezado
  try {
    ORDER_ID = await INDEX("ORDERS");
    await knex("ORDERS").insert({
      ID: ORDER_ID,
      CODE: await CODE(),
      USER_ID: req.uid,
      ADDRESS_ID: address,
      RECIPIENT_NAME: recipient_name,
      RECIPIENT_PHONE: recipient_phone,
      PAYMENT_TYPE: payment_type,
      CARD_ID: card_id,
      SUBTOTAL: subtotal,
      TAX: tax,
      CREATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
      UPDATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Error al crear el encabezado de la orden.",
    });
  }

  // Inserta detalles
  try {
    for (const detail of JSON.parse(details)) {
      await knex("ORDERDETAILS").insert({
        ID: await INDEX("ORDERDETAILS"),
        ORDER_ID,
        PRODUCT_ID: detail.product,
        QUANTITY: detail.quantity,
        AMOUNT: detail.amount,
        CREATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
        UPDATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
      });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Error al crear los detalles de la orden.",
    });
  }

  // Inserta estado inicial
  try {
    await knex("ORDER_STATUSES").insert({
      ID: await INDEX("ORDER_STATUSES"),
      ORDER_ID,
      STATUS_ID: 1,
      CREATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Error al crear los detalles de la orden.",
    });
  }

  // manda correo
  try {
    // obtiene el usuario
    const user = await knex
      .select()
      .from("USERS")
      .where({ ID: req.uid })
      .first();

    // console.log(products);

    const productsIds = JSON.parse(details).map((d) => d.product);
    const products = await knex
      .select()
      .from("PRODUCTOS")
      .whereIn("PRODUCTO", productsIds);
    const completeDetails = JSON.parse(details).map((d) => {
      return {
        ...d,
        name: products.find((p) => p.PRODUCTO == d.product).DETALLE,
      };
    });

    const sendEmail = require("../../tools/emailSender");
    const emailContent = require("../../emails/orderPlaced");
    await sendEmail(
      user.EMAIL,
      "LACTOLAC | Bienvenido.",
      emailContent(`${user.NAMES} ${user.LASTNAMES}`, completeDetails, subtotal)
    );
  } catch (error) {
    console.log(error);
  }

  return res.json({
    success: true,
    message: "Se ha ingresado la orden con éxito.",
  });
});

router.delete("/:id", async (req, res) => {
  const { id } = req.params;

  // Obtiene la orden
  const order = await knex("ORDERS").select().where({ ID: id }).first();

  // Verifica si existe
  if (!order) {
    return res
      .status(400)
      .json({ success: false, message: "La orden solicitada no existe." });
  }

  // verifica que la orden este en estado 1
  const statuses = await knex
    .select("STATUS_ID")
    .from("ORDER_STATUSES")
    .where({ ORDER_ID: order.ID })
    .orderBy("CREATED_AT", "ASC");

  if (statuses[statuses.length - 1].STATUS_ID != 1) {
    return res.status(400).json({
      success: false,
      message: "La orden no puede ser eliminada porque ya esta en procesada.",
    });
  }

  try {
    await knex("ORDERS")
      .where({ ID: id })
      .update({
        UPDATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
      });

    await knex("ORDER_STATUSES").insert({
      ID: await INDEX("ORDER_STATUSES"),
      ORDER_ID: id,
      STATUS_ID: 5,
      REASON: "Usuario lo solicito desde la app",
      CREATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
    });

    try {
      const user = await knex
        .select()
        .from("USERS")
        .where({ ID: req.uid })
        .first();

      const sendEmail = require("../tools/emailSender");
      const emailContent = require("../../emails/orderCancelled");
      await sendEmail(
        user.EMAIL,
        "LACTOLAC | Orden cancelada.",
        emailContent(`${user.NAMES} ${user.LASTNAMES}`, order.CODE)
      );
    } catch (error) {
      console.error(error);
    }

    return res.json({
      success: true,
      message: "La orden ha sido cancelada.",
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Error al eliminar la orden.",
    });
  }
});

module.exports = router;
