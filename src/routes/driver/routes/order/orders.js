const express = require("express");
const router = express.Router({ mergeParams: true });
const utcToZonedTime = require("date-fns-tz").utcToZonedTime;
const { format } = require("date-fns");

const connection = require("../../../../utils").CONNECTION;
const INDEX = require("../../../../utils").createIndex;
const checkRequired = require("../../../../utils").checkRequired;

const knex = require("knex")({
  client: "oracledb",
  connection,
});

router.post("/:id", async (req, res) => {
  // obtiene el id
  const { id } = req.params;

  // obtiene la orden
  const order = await knex("ORDERS").select().where({ ID: id }).first();

  // verifica que la orden exista
  if (!order) {
    return res
      .status(400)
      .json({ success: false, message: "La orden solicitada no existe." });
  }

  // verifica que la orden se pueda marcar como completada
  const statuses = await knex
    .select("STATUS_ID")
    .from("ORDER_STATUSES")
    .where({ ORDER_ID: order.ID })
    .orderBy("CREATED_AT", "ASC");

  if (statuses[statuses.length - 1].STATUS_ID != 3) {
    return res.status(400).json({
      success: false,
      message: "La orden solicitada no se puede marcar como completada.",
    });
  }

  // actualiza de en ruta
  try {
    await knex("ORDERS")
      .where("ID", id)
      .update({
        UPDATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
      });

    await knex("ORDER_STATUSES").insert({
      ID: await INDEX("ORDER_STATUSES"),
      ORDER_ID: id,
      STATUS_ID: 4,
      CREATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
    });

    return res.json({
      success: true,
      message: "La orden ha sido completada.",
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Error al marcar como completada la orden.",
    });
  }
});

router.delete("/:id", async (req, res) => {
  // verifica los campos requeridos
  const check = checkRequired(req.body, ["reason"]);
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message });
  }

  // obtiene la data requerida
  const { id } = req.params;
  const { reason } = req.body;

  // obtiene la orden
  const order = await knex("ORDERS").select().where({ ID: id }).first();

  // verifica que la orden exista
  if (!order) {
    return res
      .status(400)
      .json({ success: false, message: "La orden solicitada no existe." });
  }

  // verifica que la orden se pueda marcar como cancelada
  const statuses = await knex
    .select("STATUS_ID")
    .from("ORDER_STATUSES")
    .where({ ORDER_ID: order.ID })
    .orderBy("CREATED_AT", "ASC");

  if (statuses[statuses.length - 1].STATUS_ID == 3 && order.PAYMENT_TYPE == 0) {
    // actualiza orden a canelada
    try {
      await knex("ORDERS")
        .where("ID", id)
        .update({
          UPDATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
        });

      await knex("ORDER_STATUSES").insert({
        ID: await INDEX("ORDER_STATUSES"),
        ORDER_ID: id,
        STATUS_ID: 5,
        REASON: reason,
        CREATED_AT: utcToZonedTime(new Date(), "America/El_Salvador"),
      });

      return res.json({
        success: true,
        message: "La orden ha sido cancelada.",
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        success: false,
        message: "Error al marcar como cancelada la orden.",
      });
    }
  } else {
    return res.status(400).json({
      success: false,
      message: "La orden solicitada no se puede marcar como cancelada.",
    });
  }
});

router.get("/", async (req, res) => {
  const check = checkRequired(
    req.query,
    [{ name: "status", optional: true }],
    true
  );
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message });
  }

  const { status } = req.query;

  // Obtiene las ordenes
  let orders = await knex
    .select()
    .from("ORDERS")
    .where({ DRIVER_ID: req.did })
    .orderBy("CREATED_AT", "ASC");

  // para cada orden
  for (const order of orders) {
    // obtiene los detalles
    order.CARD_MASK = await knex
      .select()
      .from("USER_CARDS")
      .where({ CARD_ID: order.CARD_ID })
      .first();

    order.DETAILS = await knex
      .select(
        "ORDERDETAILS.ID",
        "ORDERDETAILS.ORDER_ID",
        "ORDERDETAILS.PRODUCT_ID",
        "ORDERDETAILS.QUANTITY",
        "ORDERDETAILS.CREATED_AT",
        "ORDERDETAILS.UPDATED_AT",
        "ORDERDETAILS.AMOUNT",
        "PRODUCTOS.DETALLE AS PRODUCT_NAME"
      )
      .from("ORDERDETAILS")
      .where({ ORDER_ID: order.ID })
      .leftJoin("PRODUCTOS", "ORDERDETAILS.PRODUCT_ID", "PRODUCTOS.PRODUCTO");

    order.CREATED_AT = format(new Date(order.CREATED_AT), "dd/MM/yyyy hh:mm a");

    // obtiene los estados
    const STATUSES = await knex
      .select("STATUS_ID", "CREATED_AT", "NAME", "COMMENTS", "REASON")
      .from("ORDER_STATUSES")
      .where({ ORDER_ID: order.ID })
      .leftJoin("STATUSES", "ORDER_STATUSES.STATUS_ID", "STATUSES.ID")
      .orderBy("CREATED_AT", "ASC");
    order.STATUSES = STATUSES;
    order.STATUS = STATUSES[STATUSES.length - 1].STATUS_ID;
    order.STATUS_NAME = STATUSES[STATUSES.length - 1].NAME;
    order.STATUS_DATETIME = format(
      new Date(STATUSES[STATUSES.length - 1].CREATED_AT),
      "dd/MM/yyyy hh:mm a"
    );

    // obtiene la direccion
    const address_details = await knex
      .select()
      .from("ADDRESSES")
      .where({ ID: order.ADDRESS_ID })
      .first();

    address_details.COUNTRY_NAME = (
      await knex
        .select()
        .from("COUNTRY")
        .where({ ID: address_details.COUNTRY_ID })
        .first()
    ).NAME;
    address_details.STATE_NAME = (
      await knex
        .select()
        .from("STATE")
        .where({ ID: address_details.STATE_ID })
        .first()
    ).NAME;
    address_details.CITY_NAME = (
      await knex
        .select()
        .from("CITY")
        .where({ ID: address_details.CITY_ID })
        .first()
    ).NAME;
    order.ADDRESS_DETAILS = address_details;
  }

  if (status) {
    switch (status) {
      case "active":
        orders = orders.filter((o) => [1, 2, 3].includes(o.STATUS));
        break;
      case "onRoute":
        orders = orders.filter((o) => [3].includes(o.STATUS));
        break;
      case "others":
        orders = orders.filter((o) => [4, 5, 6, 7].includes(o.STATUS));
        break;
    }
  }

  res.json({
    success: true,
    message: `List of orders`,
    count: orders.length,
    history: orders,
  });
});

router.get("/:id", async (req, res) => {
  const check = checkRequired(
    req.query,
    [{ name: "status", optional: true }],
    true
  );
  if (!check.success) {
    return res.status(400).json({ success: false, message: check.message });
  }

  const { status } = req.query;

  // Obtiene las ordenes
  let order = await knex
    .select()
    .from("ORDERS")
    .where({ DRIVER_ID: req.did, ID: req.params.id })
    .orderBy("CREATED_AT", "DESC")
    .first();

  order.CARD = await knex
    .select()
    .from("USER_CARDS")
    .where({ CARD_ID: order.CARD_ID })
    .first();

  order.DETAILS = await knex
    .select(
      "ORDERDETAILS.ID",
      "ORDERDETAILS.ORDER_ID",
      "ORDERDETAILS.PRODUCT_ID",
      "ORDERDETAILS.QUANTITY",
      "ORDERDETAILS.CREATED_AT",
      "ORDERDETAILS.UPDATED_AT",
      "ORDERDETAILS.AMOUNT",
      "PRODUCTOS.DETALLE AS PRODUCT_NAME"
    )
    .from("ORDERDETAILS")
    .where({ ORDER_ID: order.ID })
    .leftJoin("PRODUCTOS", "ORDERDETAILS.PRODUCT_ID", "PRODUCTOS.PRODUCTO");

  order.CREATED_AT = format(new Date(order.CREATED_AT), "dd/MM/yyyy hh:mm a");

  // obtiene los estados
  const STATUSES = await knex
    .select("STATUS_ID", "CREATED_AT", "NAME", "COMMENTS", "REASON")
    .from("ORDER_STATUSES")
    .where({ ORDER_ID: order.ID })
    .leftJoin("STATUSES", "ORDER_STATUSES.STATUS_ID", "STATUSES.ID")
    .orderBy("CREATED_AT", "ASC");
  order.STATUSES = STATUSES;
  order.STATUS = STATUSES[STATUSES.length - 1].STATUS_ID;
  order.STATUS_NAME = STATUSES[STATUSES.length - 1].NAME;
  order.STATUS_DATETIME = format(
    new Date(STATUSES[STATUSES.length - 1].CREATED_AT),
    "dd/MM/yyyy hh:mm a"
  );

  // obtiene la direccion
  const address_details = await knex
    .select()
    .from("ADDRESSES")
    .where({ ID: order.ADDRESS_ID })
    .first();

  address_details.COUNTRY_NAME = (
    await knex
      .select()
      .from("COUNTRY")
      .where({ ID: address_details.COUNTRY_ID })
      .first()
  ).NAME;
  address_details.STATE_NAME = (
    await knex
      .select()
      .from("STATE")
      .where({ ID: address_details.STATE_ID })
      .first()
  ).NAME;
  address_details.CITY_NAME = (
    await knex
      .select()
      .from("CITY")
      .where({ ID: address_details.CITY_ID })
      .first()
  ).NAME;
  order.ADDRESS_DETAILS = address_details;

  res.json({
    success: true,
    message: `Details of order`,
    order,
  });
});

module.exports = router;
