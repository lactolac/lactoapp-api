const express = require('express')
const router = express.Router()

// ROUTES
const orders = require('./orders')

router.use('/', orders)

module.exports = router