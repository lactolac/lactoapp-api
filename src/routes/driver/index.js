const express = require('express')
const router = express.Router()
const { checkDriverAuth } = require('../../utils')

// ROUTES
const auth = require('./routes/auth')
const orders = require('./routes/order')

// ROUTES FOR ADMIN
router.use('/auth', auth)
router.use('/orders', checkDriverAuth, orders)

module.exports = router