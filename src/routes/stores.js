const express = require('express')
const router = express.Router()

const connection = require('../utils').CONNECTION

const knex = require('knex')({
  client: 'oracledb', connection
});

router.get('/', async (req, res) => {
  let stores = await knex.select().from('STORE')

  res.json({
    success: true,
    message: 'List of store',
    count: stores.length,
    storeList: stores
  })
})

module.exports = router