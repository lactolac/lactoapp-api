const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const checkAuth = require('./utils').checkAuth

// ROUTES
const auth = require('./routes/auth')
const products = require('./routes/products')
const user = require('./routes/user')
const settings = require('./routes/settings')
const orders = require('./routes/order')
const stores = require('./routes/stores')

const port = 2000
const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use('/images', express.static(__dirname + '/assets/images/'));

// ROUTES FOR APP
app.use('/auth', auth)
app.use('/products', products)
app.use('/stores', stores)
app.use('/settings', settings)
app.use('/user', checkAuth, user)
app.use('/orders', checkAuth, orders)

// ROUTES FOR ADMIN
app.use('/admin', require('./routes/admin'))
app.use('/driver', require('./routes/driver'))

app.get('/', (req, res) => {
  res.status(400).send('Que ta chendo? 🤨, sáquese de aqui hombe, uchale 😒')
})

app.listen(port, () => console.log(`1 Server listening on port ${port}!`))