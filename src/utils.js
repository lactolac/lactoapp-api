const isValid = require('date-fns').isValid
const isEmpty = require('ramda').isEmpty
const jwt = require('jsonwebtoken')
const customAlphabet = require('nanoid').customAlphabet

const SECRET = 'Z9*1o0@b3UnQKRW4v01&gxeUQ3&XY^cC'
const ALPHABETH = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ-0123456789'
const connection = {
  host: "10.10.4.9",
  user: 'FACTMOVIL',
  password: 'FACLAC',
  database: 'LACTOSA',
  port: 1521,
}

const createIndex = async function (table) {
  const max = await require('knex')({
    client: 'oracledb', connection
  })(table).max('ID as ID').first()
  return max.ID + 1
}

const createCode = async function () {
  const CODE = customAlphabet(ALPHABETH, 8)()
  const existCode = await require('knex')({
    client: 'oracledb', connection
  })('ORDERS').where({ CODE }).first()
  if (existCode) {
    createCode()
  } else {
    return CODE
  }
}

const checkRequired = function (object, array, nonrequired = false) {
  const response = { success: false, message: 'Campos incompletos.' } // generic false messag

  if (isEmpty(object)) {
    if (nonrequired) {
      return { success: true }
    }
    return { success: false, message: 'Ningún campo recibido.' }
  }

  // loop over all required fields check if received
  for (const field of array) {

    // if field field is object
    if (typeof field == 'object') {
      if (object.hasOwnProperty(field.name)) {
        const checked = checkType(object[field.name], field.type)
        if (!checked.success) {
          return { success: false, message: `El campo ${field.name} tiene un formato incorrecto, ${checked.message}` }
        }
      } else {
        if (!field.optional) {
          return response
        }
      }
    } else {
      if (!object.hasOwnProperty(field)) {
        return response
      }
    }

  }
  return { success: true }
}

const checkType = (value, type) => {
  switch (type) {
    case 'date':
      const RegDate = /[0-9]{4}-[0-9]{2}-[0-9]{2}/;
      return {
        success: RegDate.test(value) && isValid(new Date(value)),
        message: 'debe ser YYYY-MM-DD'
      }
      break;
    case 'email':
      const RegEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return {
        success: RegEmail.test(value),
        message: 'Debe ser un correo válido.'
      }
      break;
    case 'float':
      break;
    case 'array':
      return {
        success: Array.isArray(JSON.parse(value)) && JSON.parse(value).length > 0,
        message: 'Debe ser un arreglo y no ser vacio'
      }
      break
    default:
      return { success: true }
      break;
  }
}

// MIDDLEWARE TO CHECK AUTHENTICATION FOR APP USERS
const checkAuth = function (req, res, next) {
  const token = req.headers.authorization

  // check if toke is provided
  if (token == null) { return res.status(401).json({ success: false, message: 'Debes iniciar sesión para poder realizar esta acción.' }) }

  try {
    const { uid } = jwt.verify(token, SECRET);
    req.uid = uid
    next()
  } catch (error) {
    // when toke not valid is provided
    return res.status(401).json({ success: false, message: 'Sesión no válida.' })
  }
}
// MIDDLEWARE TO CHECK AUTHENTICATION FOR ADMIN DHASBOARD
const checkAdminAuth = function (req, res, next) {
  const token = req.headers.authorization
  if (token == null) { return res.status(401).json({ success: false, message: 'Debes iniciar sesión para poder realizar esta acción.' }) }

  try {
    const { uid, sid } = jwt.verify(token, SECRET);
    if (sid == null) { return res.status(401).json({ success: false, message: 'Usuario no permitido en API administrativa.' }) }
    req.uid = uid
    req.sid = sid
    next()
  } catch (error) {
    // when toke not valid is provided,
    return res.status(401).json({ success: false, message: 'Sesión no válida.' })
  }
}

// MIDDLEWARE TO CHECK AUTHENTICATION FOR DRIVER APP
const checkDriverAuth = function (req, res, next) {
  const token = req.headers.authorization
  if (token == null) { return res.status(401).json({ success: false, message: 'Debes iniciar sesión para poder realizar esta acción.' }) }

  try {
    const { did } = jwt.verify(token, SECRET);
    console.log(did)
    if (did == null) { return res.status(401).json({ success: false, message: 'Usuario no permitido en API administrativa.' }) }
    req.did = did
    next()
  } catch (error) {
    // when toke not valid is provided,
    return res.status(401).json({ success: false, message: 'Sesión no válida.' })
  }
}

module.exports = {
  SECRET,
  CONNECTION: connection,
  createIndex,
  checkRequired,
  checkAuth,
  checkAdminAuth,
  checkDriverAuth,
  createCode
}